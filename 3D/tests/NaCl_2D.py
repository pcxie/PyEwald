#!/usr/bin/python3

"""
.. module:: NaCl.py
    :synopsis: Obtain the Madelung constant for NaCl sheet using the charge-charge interaction matrix.

.. moduleauthor:: D. Wang <dwang5@zoho.com>



TODO: 2D NaCl is a NaCl sheet. Using our 3D Ewald program, it may be possible to calculate its Madelung
According to C. Liu, "The calculation of Madelung constant in the two-dimensional NaCl crystal", College Physics, 14, 21 (1995),
the value is  :math:`M_0=-0.8069`.

"""

import sys
import os
sys.path.append("../src/")

import os.path
from netCDF4 import Dataset
import numpy as np
from charge_charge import Charge_charge
from dipole_dipole import Dip_dip
from configuration2D import *
from utility import *

n1 = 2
n2 = 2
n3 = 2

# ----- NaCl -----
lattice = np.array([[1,0,0],[0,1,0],[0,0,1]])
nc_file = "NaCl-matrix_{n1}x{n2}x{n3}.nc".format(n1=n1,n2=n2,n3=n3)
calculated = os.path.exists(nc_file)
if (not calculated):
    cc = Charge_charge(n1,n2,n3,lattice)
    cc.write_charge_matrix(nc_file)

#Now read the charge-charge matrix.
dataset = Dataset(nc_file,'r')
#print(dataset.dimensions.keys())
#print(dataset.dimensions['ia'])
cc_mat = dataset['matrix']

g_alloy = Charge_config(n1,n2,n3,lattice)
g_alloy.generate_high_symmetry(p=1,k=(1,1,1))
alloy_fn = "NaCl_config.nc"
g_alloy.write_alloy_matrix(alloy_fn)
rslt = calc_madelung_cc(cc_mat,alloy_fn)

print("Madelung constant (NaCl): ", rslt)
